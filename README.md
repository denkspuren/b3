# b3

Bildung, Bits und Bäume - global und lokal gedacht und gemacht


* Das [netzwerk n](https://netzwerk-n.org) ist laut [über uns, wer wir sind](https://netzwerk-n.org/ueber-uns/wer-wir-sind/) ein als Verein organisiertes "Netzwerk von überwiegend Studierenden, Initiativen, Promovierenden und jungen Berufstätigen an Hochschulen und engagiert sich für einen gesamtinstitutionellen Wandel an Hochschulen im Sinne einer nachhaltigen Entwicklung in den Bereichen Betrieb, Lehre, Forschung, Governance und Transfer." (2019-09-09)